//This login page is for Association Admin

package com.hp.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.hp.qa.base.TestBase;

@SuppressWarnings("unused")
public class LoginPage extends TestBase{

	//Page Factory - OR:
		@FindBy(name="email")
		WebElement username;
		
		@FindBy(name="password")
		WebElement password;
		
		//This login buttin is for group and association web application
		@FindBy(xpath="//*[@id=\"login_btn\"]")
		WebElement loginBtn;
		
		// This login button is for doctor web application
		/*@FindBy(xpath="/html/body/div[3]/nav/div/div/div[2]/form/table/tbody/tr[2]/td[3]/button")
		WebElement loginBtn;*/
		
		@FindBy(xpath="//img[contains(@class,'img-circle')]")
		WebElement hpLogo;
		
		//Initializing the Page Objects:
		public LoginPage(){
			PageFactory.initElements(driver, this);
		}
		
		//Actions:
		public String validateLoginPageTitle(){
			return driver.getTitle();
		}
		
		public boolean validateHpImage(){
			return hpLogo.isDisplayed();
		}
		
		public TimelinePage login(String un, String pwd){
			username.sendKeys(un);
			password.sendKeys(pwd);
			//loginBtn.click();
			    	JavascriptExecutor js = (JavascriptExecutor)driver;
			    	js.executeScript("arguments[0].click();", loginBtn);			
			return new TimelinePage();
		}
}
