package com.hp.association.testcase;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hp.qa.base.TestBase;
import com.hp.qa.pages.LoginPage;
import com.hp.qa.pages.TimelinePage;

import javafx.beans.property.SetProperty;

public class AssoDiscussionTest extends TestBase {
	
	private static final Integer Beforecount = null;
	private static final Integer aftercount = null;
	LoginPage loginpg;
	TimelinePage timelinepg;
	
	public AssoDiscussionTest() {
		super();
	}
	
	@BeforeMethod
	public void setup() throws InterruptedException {
		initalize();
		loginpg = new LoginPage();
		timelinepg = loginpg.login(prop.getProperty("username"), prop.getProperty("password"));
		
		//Click on discussion tab
		WebElement discussion = driver.findElement(By.xpath("//*[@id=\"pageContainer\"]/div[1]/div/ul/li[3]/a/span"));
		discussion.click();		
	}
	
	@Test(priority=1)
	public void DiscusiontabTest() throws InterruptedException {
		
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		//verify the page
		String Expected = "Discussions";
		String Actual = driver.findElement(By.xpath("//*[@id=\"pageContainer\"]/div[2]/div[2]/div/ul/li/b")).getText();
		System.out.println("This is title: "+Actual);
		Assert.assertEquals(Expected, Actual);		
	}
	
	@Test(priority=2)
	public void CreateDiscussionTest() throws InterruptedException {
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		//Write something 
		WebElement post = driver.findElement(By.xpath("//*[@id=\"description\"]"));
		post.sendKeys("This is testing discussion from admin by using selenium....!");
			
		//Click on add image button
		WebElement attach_image = driver.findElement(By.xpath("//*[@id=\"discussionUploadImage\"]"));
				
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).build().perform();
				
		//Uploading the image
		WebElement image1 = driver.findElement(By.xpath("//*[@id=\"theFile\"]"));
		image1.sendKeys("/home/akshay/Videos/1.jpg");
				
		System.out.println("Image 1 uploaded");
				
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
							
		//Enter title for image
		System.out.println("Image title!");
		WebElement imgtitle = driver.findElement(By.xpath("//*[@id=\"image_desc\"]"));
		imgtitle.sendKeys("This is image title");
				
		//After upload click on done button
		WebElement Done = driver.findElement(By.xpath("//*[@id=\"btnUpload-Images\"]"));
		Done.click();
				
		//Publishing the post
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		WebElement post1 = driver.findElement(By.id("discussionSubmit"));
		post1.click();
				
		System.out.println("Post uploaded successfully....!");
			
	}
	
	@Test(priority=3)
	public void LikeDislikeTest() throws InterruptedException, AWTException {
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		
		// Like and dislike Functionality
		String like_state = driver.findElement(By.xpath("//*[@id=\"current_state\"]")).getAttribute("value"),
			   like = "unliked";
		WebElement likebtn = driver.findElement(By.xpath("//*[@id=\"upvoteDiv\"]"));

		String liked="liked";//liked_state = driver.findElement(By.xpath("//*[@id=\"current_state\"]")).getAttribute("value"),
		
		WebElement likedbtn = driver.findElement(By.id("upvotedDiv"));
	
		Thread.sleep(8000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		
		//If-Else for like and unlike post		
		if( like_state.equals(like)) {
			System.out.println("like post working");			
			likebtn.click();
			Thread.sleep(8000);
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
			Assert.assertEquals(liked,like_state);
			System.out.println("post like successfully...!");
		}
				
		else if(like_state.equals(liked)){
			System.out.println("unlike post working");
			likedbtn.click();
			Thread.sleep(8000);
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
			Assert.assertEquals(like,like_state);
			System.out.println("Post unlike successfully...!");
		}
				
		else {
			System.out.println("Failed to like/Unlike post");
		}
		
		System.out.println("Post status= "+like_state);
		
		//If-Else for like and unlike count
		if(like_state.equals(like)) {
			String before_like_count = driver.findElement(By.xpath("//*[@id=\"associationPaginationHandler\"]/div[1]/div/div[6]/div/div/div[1]/p/a")).getText();
		
			System.out.println("Before like count is :  "+before_like_count);
			String countStr[] = before_like_count.split(" ");
			Integer Beforecount = Integer.valueOf(countStr[0]);		
						
		}
		else if(like_state.equals(liked)){
			String After_like_count = driver.findElement(By.xpath("//*[@id=\"associationPaginationHandler\"]/div[1]/div/div[6]/div/div/div[1]/p/a")).getText();
			System.out.println("After like count is :  "+After_like_count);
							
			String AftercountStr[] = After_like_count.split(" ");
			Integer aftercount = Integer.valueOf(AftercountStr[0]);				
		}
		    
		//Condition for check before and after count
		if(aftercount<Beforecount) {
		   System.out.println("post is disliked....!");
		   System.out.println("Count is: "+aftercount);
		}
		else if(aftercount>Beforecount) {
		   System.out.println("post is liked.....!");
		   System.out.println("Count is: "+Beforecount);
		}
		else {
		   System.out.println("Like button is not working...!");
		}
				
		String after_liked = driver.findElement(By.xpath("//*[@id=\"upvotedDiv\"]")).getText();
		System.out.println("Now text is = "+liked);
		Assert.assertEquals(after_liked, after_liked);
		System.out.println("Assertion is complete...!");		
	}
	
	@Test(priority=4)
	public void cmtreplyTest() throws InterruptedException, AWTException {
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		
		WebElement cmtbtn = driver.findElement(By.cssSelector("#associationPaginationHandler > div.post-timeline > div:nth-child(1) > div.row.feed-like-comment-share.row-margin-0 > div > ul > li.feed-action-list.addComment > a"));
		cmtbtn.click();
		
		Thread.sleep(8000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		System.out.println("Comment button clicked..!");
		
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_F5);
		robot.keyRelease(KeyEvent.VK_F5);
		
		WebElement write_cmt = driver.findElement(By.xpath("//*[@id=\"txtCommentBox\"]"));
		write_cmt.sendKeys("This is comment from web automation test..!");
		write_cmt.sendKeys(Keys.ENTER);
		Thread.sleep(8000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		System.out.println("Comment Entered..!");
		
		Robot robot1 = new Robot();
		robot1.keyPress(KeyEvent.VK_F5);
		robot1.keyRelease(KeyEvent.VK_F5);
				
		WebElement reply = driver.findElement(By.xpath("//*[@id=\"reply-action\"]/div[2]/p/a"));
		reply.click();
		Thread.sleep(8000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		WebElement wrt_reply = driver.findElement(By.xpath("//*[@id=\"txtReplyCommentBox\"]"));
		wrt_reply.sendKeys("This is reply from web automation");
		wrt_reply.sendKeys(Keys.ENTER);
		Thread.sleep(8000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		System.out.println("Reply Entered..!");
	}
	
	@AfterMethod
	public void tearDown() {
		driver.close();
	}
}
