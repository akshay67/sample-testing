package com.hp.association.testcase;

import org.testng.annotations.Test;
import com.hp.qa.base.TestBase;
import com.hp.qa.pages.LoginPage;
import com.hp.qa.pages.TimelinePage;

import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.selenesedriver.FindElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;

public class AssoCasesTest extends TestBase{
  
	LoginPage loginpage;
	TimelinePage timelinepage;
	
	public AssoCasesTest() {
		super();
	}
	
	@BeforeMethod
	  public void beforeMethod() {
		initalize();
		loginpage = new LoginPage();
		timelinepage = loginpage.login(prop.getProperty("username"),prop.getProperty("password"));
		WebElement casestab = driver.findElement(By.xpath("//*[@id=\"pageContainer\"]/div[1]/div/ul/li[4]/a/span"));
		casestab.click();
	  }

	@SuppressWarnings("unlikely-arg-type")
	@Test(priority=1)
	public void CreateCaseTest() throws InterruptedException {
		//Create new case button
		WebElement createcase = driver.findElement(By.xpath("//*[@id=\"pageContainer\"]/div[2]/div[2]/div[2]/div/a/button"));
		createcase.click();
		
		// Verify to open create case page open or not
		String Expected_title = "Create Case";
		String Actual_title = driver.findElement(By.xpath("//*[@id=\"pageContainer\"]/div/form/div[1]/div[1]/p/b")).getText();
		assertEquals(Actual_title, Expected_title);
		System.out.println("Page opened successfully...!!");
		
		//Fill Information to create case 
		WebElement c_title = driver.findElement(By.xpath("//*[@id=\"title\"]"));
		WebElement c_courtesy = driver.findElement(By.xpath("//*[@id=\"courtesy\"]"));
		WebElement c_description = driver.findElement(By.xpath("//*[@id=\"description\"]"));
				
		c_title.sendKeys("This is case title..!!!");
		c_courtesy.sendKeys("This is case courtesy");
		c_description.sendKeys("This is the case description for this case and this is case is for testing purpose...!, This description may be little bit long because descriptions are always long...!!");
		
		//Image multiple upload
		WebElement c_image = driver.findElement(By.xpath("//*[@id=\"theFile\"]"));
		// images path will change as per your machine path
		c_image.sendKeys("/home/akshay/Videos/4.jpeg \n /home/akshay/Videos/3.jpg \n /home/akshay/Videos/2.jpg");

		Thread.sleep(10000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		
		WebElement proceed_btn = driver.findElement(By.xpath("//*[@id=\"btnProceedOnCaseDetails\"]"));
		proceed_btn.click();
		
		// Select the tags
		WebElement tag1 = driver.findElement(By.xpath("//*[@id=\"addCaseTagsContainer\"]/div/div[1]/div/form/div[2]/div/div[3]/div/div/div/div/p[4]"));
		WebElement tag2 = driver.findElement(By.xpath("//*[@id=\"addCaseTagsContainer\"]/div/div[1]/div/form/div[2]/div/div[3]/div/div/div/div/p[8]"));
		WebElement tag3 = driver.findElement(By.xpath("//*[@id=\"addCaseTagsContainer\"]/div/div[1]/div/form/div[2]/div/div[3]/div/div/div/div/p[9]"));
		tag1.click();
		tag2.click();
		tag3.click();
		
		WebElement proceed = driver.findElement(By.xpath("//*[@id=\"btnProceedCase\"]"));
		proceed.click();
		
		//Click on dropdown menu and select option 
		WebDriverWait wait = new WebDriverWait(driver, 10);

		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("select#visibility.form-control.select")));
		element.click();
				 
		WebElement selectop = driver.findElement(By.xpath("//*[@id=\"visibility\"]/option[3]"));
		selectop.click();
				 
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE);
		
		// Publish the case
		WebElement publish = driver.findElement(By.xpath("//*[@id=\"btnProceedOnTagsPage\"]"));
		publish.click();
		
		System.out.println("Case posted successfully...!");
	  }
	
	@Test(priority=2)
	public void CaseViewTest() throws InterruptedException, AWTException {
		// View case detials 
		WebElement viewcase = driver.findElement(By.xpath("//*[@id=\"btnViewDetails\"]"));
		viewcase.click();
		
		String actual = driver.findElement(By.xpath("//*[@id=\"pageContainer\"]/div/div/div[1]/div/p/b")).getText();
		String expected = "Case Details";
		assertEquals(actual, expected);
		System.out.println("Case details page open successfully...!!!");
		
		WebElement write_cmt = driver.findElement(By.xpath("//*[@id=\"txtCommentBox\"]"));
		write_cmt.sendKeys("This comment is from after clicking on view details button");
		write_cmt.sendKeys(Keys.ENTER);
		Thread.sleep(8000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		System.out.println("Comment Entered..!");
		
		Robot robot1 = new Robot();
		robot1.keyPress(KeyEvent.VK_F5);
		robot1.keyRelease(KeyEvent.VK_F5);
				
		WebElement reply = driver.findElement(By.xpath("//*[@id=\"reply-action\"]/div[2]/p/a"));
		reply.click();
		Thread.sleep(8000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		WebElement wrt_reply = driver.findElement(By.xpath("//*[@id=\"txtReplyCommentBox\"]"));
		wrt_reply.sendKeys("This is reply from web automation");
		wrt_reply.sendKeys(Keys.ENTER);
		Thread.sleep(8000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		System.out.println("Reply Entered..!");
		
	}
	
	@Test(priority = 3)
	public void OperationsCaseTest() throws InterruptedException {
		// Like and unlike the case
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		
		// Like and dislike Functionality
		String like_state = driver.findElement(By.xpath("//*[@id=\"current_state\"]")).getAttribute("value");
		String unlike = "unliked";
		WebElement likebtn = driver.findElement(By.xpath("//*[@id=\"upvoteDiv\"]"));

		String liked="liked";//liked_state = driver.findElement(By.xpath("//*[@id=\"current_state\"]")).getAttribute("value"),
		
		WebElement likedbtn = driver.findElement(By.id("upvotedDiv"));
	
		Thread.sleep(8000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		
		//If-Else for like and unlike post		
		if(like_state.equals(unlike)) {
			System.out.println("like post working");			
			likebtn.click();
			Thread.sleep(8000);
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
			Assert.assertEquals(liked,like_state);
			System.out.println("post like successfully...!");
		}
		
		else if(like_state.equals(liked)){
			System.out.println("unlike post working");
			likedbtn.click();
			Thread.sleep(8000);
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
			Assert.assertEquals(unlike,like_state);
			System.out.println("Post unlike successfully...!");
		}
				
		else {
			System.out.println("Failed to like/Unlike post");
		}
		
		System.out.println("Post status= "+like_state);

	}
  
	@AfterMethod
	public void afterMethod() throws InterruptedException {
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.quit();
	  }

}
