package com.hp.association.testcase;

import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hp.qa.base.TestBase;
import com.hp.qa.pages.LoginPage;
import com.hp.qa.pages.TimelinePage;

public class LoginPageTest extends TestBase {

	LoginPage loginPage;
	TimelinePage timelinePage;
	
	public LoginPageTest(){
		super();
	}
	
	@BeforeMethod
	public void setUp(){
		initalize();
		loginPage = new LoginPage();	
	}
	
	@Test(priority=1)
	public void loginPageTitleTest(){
		String title = loginPage.validateLoginPageTitle();
		System.out.println("Hey this is title:-  "+title);
		Assert.assertEquals(title, "Healthpole");
	}
		
	@Test(priority=3)
	public void loginTest(){
		timelinePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
	}
	
	@AfterMethod	
	public void tearDown(){
		driver.quit();
	}
	
}
